'use strict';


/**
*
*
*/

import mongoose from 'mongoose';

module.exports= function(done){
  const debug=$.createDebug('init: mongodb');
  debug('connect to MongoDb..');
  const conn=mongoose.createConnection('127.0.0.1:27017');
  $.mongodb=conn;
  $.model={};

  const ObjectId=mongoose.Types.ObjectId;
  $.utils.ObjectId=ObjectId;

  done();
};
