'use strict';

import mongoose from 'mongoose';


module.exports = function(done){
  const Schema = mongoose.Schema;
  const ObjectId=Schema.ObjectId;

  const Topic = new Schema({
    autor:{type:ObjectId, index: true, ref: 'User'},
    title:{type:String, trim:true},
    content:{type:String},
    tags:[{type:String, index:true}],
    createdAt:{type:Date,index:true},
    updatedAt:{type:Date,Index:true},
    lastCommentedAt:{type:Date,Index:true},
    comments:[{
      author:{type:ObjectId,ref:'User'},
      content:String,
      createdAt:Date,
    }],
    pageView:{type:Number},
  });

  $.mongodb.model('Topic',Topic);
  $.model.Topic=$.mongodb.model('Topic');
  done();
};
