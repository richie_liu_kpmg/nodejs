/**
 * partice Node.js project
 *
 * @author lalal
 */

import path from 'path';
import ProjectCore from 'project-core';
import createDebug from 'debug';

const $ = global.$= new ProjectCore();

// create debug function
$.createDebug=function(name){
  return createDebug('my:'+name);
};
const debug=$.createDebug('server');

// load config

$.init.add((done)=>{
  $.config.load(path.resolve(__dirname,'config.js'));
  const env=process.env.NODE_ENV||null;

  if (env){
    try{
      debug('load env:%s',env);
      $.config.load(path.resolve(__dirname,'../config', env+'.js'));
    }catch(err){

        debug('load env:%s fail because %s',env,err);
    }
  }
  $.env=env;

  done();
});

//init mongodb

$.init.load(path.resolve(__dirname,'init','mongodb.js'))
//load model js
//
$.init.load(path.resolve(__dirname,'models'));
//
// //load methods
//
$.init.load(path.resolve(__dirname,'methods'));
//
// // 初始化Express
$.init.load(path.resolve(__dirname, 'init', 'express.js'));

// // 初始化中间件

$.init.load(path.resolve(__dirname, 'middlewares'));
// // 加载路由
$.init.load(path.resolve(__dirname, 'routes'));
//

// 初始化limiter
$.init.load(path.resolve(__dirname, 'init', 'limiter.js'));
//init

$.init((err)=>{
  if(err){
    console.error(err);
    process.exit(-1);
  }else{
    console.log('inited, [env=%s]',$.env);
  };
});
